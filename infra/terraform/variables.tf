variable "private_key_path" {
  type = string
  default = "~/.ssh"
}

variable "public_key" {
  type = string
  default = "~/.ssh/aws.pub"
}

variable "credentials_file" {
    type = string
    default = "~/.aws/credentials"
}

variable "key_name_aws" {
    type = string
    default = "aws"
}

variable "domain_name" {
    type = string
    default = "kovalenko.website"
}
