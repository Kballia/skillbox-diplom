# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "eu-central-1"
  shared_credentials_file = var.credentials_file

}
# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = ["22", "80"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web access for Application"
  }
}

 resource "aws_key_pair" "deployer" {
   key_name   = var.key_name_aws
   public_key = "${file(var.public_key)}"
 }

# Создаём Launch Configuration - это сущность, которая определяет конфигурацию запускаемых серверов. Размер, , 

resource "aws_launch_configuration" "web" {
  #name_prefix     = "NGINX-"
  # какой будет использоваться образ
  image_id        = data.aws_ami.ubuntu.id
  # Размер машины (CPU и память)
  instance_type   = "t2.micro"
  # какие права доступа
  security_groups = [aws_security_group.web.id]
  # какие следует запустить скрипты при создании сервера
  #user_data       = file("user_data.sh")
  # какой SSH ключ будет использоваться 
  key_name = var.key_name_aws
  # Если мы решим обновить инстанс, то, прежде, чем удалится старый инстанс, который больше не нужен, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }
}


# Elastic Load Balancer проксирует трафик на наши сервера 
resource "aws_elb" "web" {
  name               = "ELB"
  # перенаправляет трафик на несколько Дата центров
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1], data.aws_availability_zones.available.names[2]]
  security_groups    = [aws_security_group.web.id]
  # слушает на порту 80
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }
  
  instances                   = [aws_instance.my_webserver.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "ELB"
  }
}

#Перенаправляем трафик от домена к elb
resource "aws_route53_record" "www" {
  zone_id = "Z061363715QKDQ18XTKC0"
  name    = var.domain_name
  type    = "A"
  
  alias {
    name                   = aws_elb.web.dns_name
    zone_id                = aws_elb.web.zone_id
    evaluate_target_health = true
  }
}


# Созаём подсети в разных Дата центрах
resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

resource "aws_default_subnet" "availability_zone_3" {
  availability_zone = data.aws_availability_zones.available.names[2]
}


# Запускаем инстанс
resource "aws_instance" "my_webserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name = var.key_name_aws
  tags = {
    Name  = "Server"
    Env = "Production"
    Tier = "Backend"
    CM = "Ansible"
  }

  lifecycle {
    create_before_destroy = true
  }

}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.my_webserver.public_ip
}

resource "local_file" "AnsibleInventory" {
 content = templatefile("inventory.tmpl",
 {
  server-name = aws_route53_record.www.name,
  server-ip = aws_instance.my_webserver.public_ip,
  server-id = aws_instance.my_webserver.id,
  private_key_path = var.private_key_path
  key_name = var.key_name_aws
 }
 )
 filename = "../ansible/inventory"
}

resource "local_file" "AnsibleInventory2" {
 content = templatefile("inventory.tmpl",
 {
  server-name = aws_route53_record.www.name,
  server-ip = aws_instance.my_webserver.public_ip,
  server-id = aws_instance.my_webserver.id,
  private_key_path = var.private_key_path
  key_name = var.key_name_aws
 }
 )
 filename = "../../service/inventory"
}